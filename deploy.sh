#!/bin/env bash

cp build/cpupolicy /usr/local/bin/cpupolicy &&
cp rofi-cpupolicy.sh /usr/local/bin/rofi-cpupolicy.sh
cp cpupolicy.service /etc/systemd/system/cpupolicy.service &&
chown root:root /usr/local/bin/cpupolicy &&
chmod +x /usr/local/bin/cpupolicy &&
chmod +s /usr/local/bin/cpupolicy && #Suid to allow users to set cpu policy
systemctl daemon-reload &&
systemctl enable cpupolicy.service > /dev/null
