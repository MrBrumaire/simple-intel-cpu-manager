module Main (main) where

import Data.Maybe
import System.Environment
import System.Exit
import System.Process (createProcess, proc)

-- Types

type MinFrequency = String

type MaxFrequency = String

data Governor = Performance | PowerSave deriving (Enum, Eq)

data CpuPolicy = CpuPolicy MinFrequency MaxFrequency Governor

-- Constantes
commandDoc = "Politiques CPU possibles : batterie, bureautique, equilibre, performance, max_perfomance."

batteriePolicy = CpuPolicy "800MHz" "1000MHz" PowerSave

bureatiquePolicy = CpuPolicy "800MHz" "1600MHz" PowerSave

equilibrePolicy = CpuPolicy "800MHz" "2500MHz" PowerSave

performancePolicy = CpuPolicy "1200MHz" "3000MHz" Performance

maxPerformancePolicy = CpuPolicy "2000MHz" "4500MHz" Performance

-- Helpers

governorToString :: Governor -> String
governorToString gov
  | gov == Performance = "performance"
  | otherwise = "powersave"

matchUserInput :: String -> Maybe CpuPolicy
matchUserInput input
  | input == "batterie" = Just batteriePolicy
  | input == "bureautique" = Just bureatiquePolicy
  | input == "equilibre" = Just equilibrePolicy
  | input == "performance" = Just performancePolicy
  | input == "max_performance" = Just maxPerformancePolicy
  | otherwise = Nothing

-- Main function
main = do
  args <- getArgs
  let maybeProfile = matchUserInput $ head args
  if Data.Maybe.isJust maybeProfile
    then do
      putStrLn $ show (head args) ++ " application de la politique CPU..."
      let profile = (\(Just a) -> a) maybeProfile
      let minFrequency = (\(CpuPolicy minFrequency _ _) -> minFrequency) profile
      let maxFrequency = (\(CpuPolicy _ maxFrequency _) -> maxFrequency) profile
      let governor = (\(CpuPolicy _ _ governor) -> governorToString governor) profile
      minFRes <- createProcess (proc "cpupower" ["frequency-set", "--min", minFrequency])
      maxFRes <- createProcess (proc "cpupower" ["frequency-set", "--max", maxFrequency])
      govFRes <- createProcess (proc "cpupower" ["frequency-set", "--governor", governor])
      info <- createProcess (proc "cpupower" ["frequency-info"])
      putStrLn "La politique CPU a été appliquée avec succès."
      exitSuccess
    else do
      putStrLn $ show (head args) ++ " n'est pas une option valide. " ++ commandDoc
      exitFailure