#!/bin/env bash
mkdir build 2> /dev/null
ghc -O main.hs -outputdir build &&
mv main build/cpupolicy &&
rm build/Main.o &&
rm build/Main.hi
