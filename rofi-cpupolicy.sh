#!/bin/env bash
CHOICE=$(echo -n -e "batterie\nbureautique\nequilibre\nperformance\nmax_performance" \
| rofi -dmenu -p "Sélectionnez la politque CPU à appliquer" \
| tr -d "\n") && \
cpupolicy $CHOICE &&
notify-send "La politique CPU a bien été appliquée" || 
notify-send "Une erreur s'est produite. La politique CPU n'a pas été appliquée"